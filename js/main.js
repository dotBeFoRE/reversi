/**
 * @type {Number}
 */
let index = 0;

/**
 * Add log entry
 * @param {string} msg Message to display
 * @param {string} type success or danger
 */
function log(msg, type) {
  localStorage.setItem(index, JSON.stringify({
    msg,
    type,
  }));
  index = (index + 1) % 9;
}

class FeedbackWidget {
  constructor(elementId) {
    this.pElementId = elementId;
  }

  get elementId() {
    return this.elementId;
  }

  /**
   * Show a message box
   * @param {string} msg Message to display
   * @param {string} type success or danger
   */
  show(msg, type) {
    this.hide();
    const el = document.getElementById(this.pElementId);
    el.textContent = msg;
    el.style.display = 'block';
    el.classList.add(`alert-${type}`);
    log(msg, type);
  }

  hide() {
    const el = document.getElementById(this.pElementId);
    el.textContent = '';
    el.classList.remove('alert-success');
    el.classList.remove('alert-danger');
    el.style.display = 'none';
  }
}

/*
function removeLog() {
  for (let i = 0; i < 10; i += 1) {
    localStorage.removeItem(i);
  }
}

function history() {
  for (let i = 0; i < 10; i += 1) {
    const msg = JSON.parse(localStorage.getItem(i));
    if (msg && msg.type && msg.msg) {
      console.log(`<type ${msg.type}> - ${msg.msg} \n`);
    }
  }
}
*/

const feedback = new FeedbackWidget('feedback');

document.getElementById('danger').addEventListener('click', () => {
  feedback.show('Nah ah', 'danger');
});

document.getElementById('success').addEventListener('click', () => {
  feedback.show('Jippie', 'success');
});

/**
  * @typedef GameConfig
  * @property [string] url Url to the api
  */

/**
 * Game module
 * @module Game
 */
const Game = (function GameModuleConstructor(url) {
  /** @type {GameConfig} */
  const config = {
    url,
  };
  /**
   * Executes when init is completed
   * @callback initCallback
   */

  /**
   * @function init Initializes the Game module
   * @param {initCallback} callback
   * @returns {string} Returns url
   */
  function init(callback) {
    callback();
    return config.url;
  }

  return {
    init,
  };
}('/game'));

/**
 * @module Game:Reversi
 */
const Reversi = (function ReversiModuleConstructor(url) {
  /** @type {GameConfig} */
  const config = {
    url,
  };

  /**
   * @function init Initializes the Game module
   * @param {initCallback} callback
   * @returns {string} Returns url
   */
  function init(callback) {
    callback();
    return config.url;
  }

  return {
    init,
  };
}('/game/reversi'));

/**
 * @module Game:Model
 */
const Model = (function ModelModuleConstructor(url) {
  /** @type {GameConfig} */
  const config = {
    url,
  };

  /**
   * @function init Initializes the Game module
   * @param {initCallback} callback
   * @returns {string} Returns url
   */
  function init(callback) {
    callback();
    return config.url;
  }

  async function getWeather() {
    const data = await Game.Data.get();
    if (data.main && data.main.temp) {
      return data;
    }

    return new Error('No temp data');
  }

  return {
    init,
    getWeather,
  };
}('/game/models'));

/**
 * Data module
 * @module Game:Data
 */
const Data = (function DataModuleConstructor() {
  /**
   * @typedef Mock
   * @property {string} url url to mock data
   * @property {object} data mock data
   */

  /**
   * @typedef DataConfig
   * @property {Mock[]} mock
   */
  /** @type {DataConfig} */
  const config = {
    mock: [
      {
        url: '/api/Spel/Beurt',
        data: 0,
      },
    ],
  };

  /**
   * @readonly
   * @enum {string}
   */
  // eslint-disable-next-line no-unused-vars
  const EnvironmentEnum = {
    DEVELOPMENT: 'development',
    PRODUCTION: 'production',
  };

  /**
   * @typedef StateMap;
   * @property {EnvironmentEnum} environment
   */

  /** @type {StateMap} */
  const stateMap = {
    environment: EnvironmentEnum.DEVELOPMENT,
  };

  /**
   * @function init Initializes the Game module
   * @param {initCallback} callback
   * @returns {string} Returns url
   */
  function init(callback) {
    callback();
    return config;
  }

  /**
   * Get data from url
   * @param {string} url Url to retrieve data from
   * @throws {Error} When not ok
   * @returns {Promise<any>}
   */
  function get(url) {
    return new Promise((resolve, reject) => {
      if (stateMap.environment === EnvironmentEnum.DEVELOPMENT) {
        const mock = config.mock.find((mockData) => mockData.url === url);
        if (mock) {
          resolve(mock.data);
        } else {
          reject(new Error('404 not found'));
        }
      } else {
        reject(new Error('not implementend'));
      }
    });
  }

  return {
    init,
    get,
  };
}());

Game.Reversi = Reversi;
Game.Model = Model;
Game.Data = Data;

Game.Data.get('/api/Spel/Beur').then(console.log);
